# Jaded-derive

Derive macro for the jaded Java deserialization library to support the 'derive'
feature.
Code should not have to depend on this crate directly, but should use
[jaded](https://crates.io/crates/jaded) instead.

