use trybuild::TestCases;

#[test]
fn error_messages() {
    let t = TestCases::new();
    t.compile_fail("tests/ui/*_fail.rs");
}

#[test]
fn successful_derivation() {
    let t = TestCases::new();
    t.pass("tests/ui/*_pass.rs");
}
