use jaded_derive::FromJava;

/// class attribute should be used in the form
/// #[jaded(class = "com.example.FooBar")]
/// Attempting to use it as either a Path or list style attribute should be a
/// compile error but should show a suggestion to use the correct style.

#[derive(FromJava)]
#[jaded(class("com.example.FooBar"))]
struct FooBar {
    foo: String,
    bar: i32,
}

#[derive(FromJava)]
#[jaded(class)]
struct BarFoo {
    foo: String,
    bar: i32,
}

pub fn main() {}
