use jaded_derive::FromJava;

/// from attribute should be used in the form
/// #[jaded(from = "path::to::Alternate")]
/// Attempting to use it as either a Path or list style attribute should be a
/// compile error but should show a suggestion to use the correct style.

mod path {
    pub mod to {
        #[derive(::jaded_derive::FromJava)]
        pub struct Alternate;

        impl From<Alternate> for super::super::FromLiteralValue {
            fn from(a: Alternate) -> super::super::FromLiteralValue {
                Self {
                    foo: String::new(),
                    bar: 211,
                }
            }
        }
    }
}

// This is the correct version
#[derive(FromJava)]
#[jaded(from = "path::to::Alternate")]
struct FromLiteralValue {
    foo: String,
    bar: i32,
}

#[derive(FromJava)]
#[jaded(from = path::to::Alternate)]
struct FromUnparsable {
    foo: String,
    bar: i32,
}

#[derive(FromJava)]
#[jaded(from)]
struct FromArgless {
    foo: String,
    bar: i32,
}

#[derive(FromJava)]
#[jaded(from(one, two))]
struct FromMultiple {
    foo: String,
    bar: i32,
}

#[derive(FromJava)]
#[jaded(from("path::to::Alternate"))]
struct FromLiteralList {
    foo: String,
    bar: i32,
}


pub fn main() {}
