use jaded_derive::FromJava;

/// rename attribute should be used in the form
/// #[jaded(rename)]
/// Attempting to pass a boolean value should be a
/// compile error but should show a suggestion to use the correct style.

#[derive(FromJava)]
#[jaded(rename = true)]
struct FooBar {
    foo: String,
    bar: i32,
}

#[derive(FromJava)]
#[jaded(rename(true))]
struct BarFoo {
    foo: String,
    bar: i32,
}

pub fn main() {}
