use jaded_derive::FromJava;

#[derive(FromJava)]
struct Path {
    #[jaded(extract)]
    foo: String,
}

#[derive(FromJava)]
struct EmptyList {
    #[jaded(extract())]
    foo: String,
}

#[derive(FromJava)]
struct TooMany {
    #[jaded(extract(path::to::method, 2, "foo"))]
    foo: String,
}

#[derive(FromJava)]
struct TooManyAndWrong {
    #[jaded(extract("path::to::method", 2, 12))]
    foo: String,
}

#[derive(FromJava)]
struct NotMethod {
    #[jaded(extract("method_name"))]
    foo: String,
}

#[derive(FromJava)]
struct NotNumber {
    #[jaded(extract(method, "0"))]
    foo: String,
}

#[derive(FromJava)]
struct MultipleErrors {
    #[jaded(extract(method, 2, 1))]
    foo: String,
    #[jaded(extract("method2", 1))]
    bar: i32,
}

#[derive(FromJava)]
struct NegativeNumber {
    #[jaded(extract(method, -2))]
    foo: String,
}

pub fn main() {}
