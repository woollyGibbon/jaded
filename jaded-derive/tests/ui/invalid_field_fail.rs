use jaded_derive::FromJava;

#[derive(FromJava)]
struct FooBar {
    foo: String,
    bar: Bar,
}

struct Bar;

pub fn main() {}
