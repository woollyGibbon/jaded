use jaded_derive::FromJava;

#[derive(FromJava)]
struct NonLiteral {
    #[jaded(field = bar)]
    foo: String,
}

#[derive(FromJava)]
struct AsList {
    #[jaded(field("bar"))]
    foo: String,
}

#[derive(FromJava)]
struct NotAString {
    #[jaded(field = 23)]
    foo: String,
}

#[derive(FromJava)]
struct NoValue {
    #[jaded(field)]
    foo: String,
}

pub fn main() {}
