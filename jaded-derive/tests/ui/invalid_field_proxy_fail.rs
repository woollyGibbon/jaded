use jaded_derive::FromJava;

#[derive(FromJava)]
struct NoValue {
    #[jaded(from)]
    foo: String,
}

#[derive(FromJava)]
struct AsPath {
    #[jaded(from("bar"))]
    foo: String,
}

#[derive(FromJava)]
struct InvalidType {
    #[jaded(from = "not valid type")]
    foo: String,
    #[jaded(from = "Valid<Type>")]
    bar: String,
}

#[derive(FromJava)]
struct NotAString {
    #[jaded(from = 23)]
    foo: String,
}

#[derive(FromJava)]
struct MultipleErrors {
    #[jaded(from = 23)]
    foo: String,
    #[jaded(from = "invalid type")]
    bar: String,
    #[jaded(from)]
    fizz: String,
}


pub fn main() {}

