use jaded_derive::FromJava;

#[derive(FromJava)]
struct AllDefaultNamed {
    foo: String,
    bar: i32,
}

#[derive(Debug, FromJava)]
struct AllDefaultUnnamed ( String );

pub fn main() { }
