use jaded_derive::FromJava;
#[derive(FromJava)]
#[jaded(unknown = "foobar?")]
struct FooBar {
    foo: String,
    bar: i32,
}

pub fn main() {}
